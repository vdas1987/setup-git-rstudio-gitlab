# Setup Git RStudio GitLab

Instructions on setting up your computer to be able to work with Git, GitLab and RStudio.

Lots of information was taken and adapted from https://happygitwithr.com

Thanks go to Jenny Bryan, the STAT 545 TAs and Jim Hester for the great book!


# Please download and install:

## R

- Go to https://cloud.r-project.org
- Click "Download R for <your OS>"
- choose option base
- Install

## RStudio

- Go to https://www.rstudio.com/
- Click "Download RStudio"
- Download RStudio Desktop (Open Source License) for your OS
- Install

![](fig/01-rstudio-draw.png)


## Git 

The following instructions are taken and adapted for you from [Happy Git with R](http://happygitwithr.com/install-git.html). Note that I was only able to test
the instructions for Windows. If you run into issues please ask your IT or a knowledgeable friend for help. 

If there's any chance it's installed already, verify that, rejoice, and skip this step.

Otherwise, find installation instructions below for your operating system.

### Git already installed?

Go to the `Terminal` in RStudio (See image below). Enter `git --version` to see your git version (if any):
 
```
git --version
```

If you are successful, that's great! You have Git already. No need to install! Move on.

If, instead, you see something more like `git: command not found` (or what you see in the image in German), keep reading.

![](fig/PREP-check-git-version.PNG)

Mac OS users might get an immediate offer to install command line developer tools. Yes, you should accept! Click "Install" and read more below.

### Windows  

Install [Git for Windows](https://git-for-windows.github.io/), also known as `msysgit` or "Git Bash", to get Git in addition to some other useful tools, such as the Bash shell. Yes, all those names are totally confusing, but you might encounter them elsewhere and I want you to be well-informed.

We like this because Git for Windows leaves the Git executable in a conventional location, which will help you and other programs, e.g. RStudio, find it and use it. This also supports a transition to more expert use, because the "Git Bash" shell will be useful as you venture outside of R/RStudio.

  *  **NOTE:** Select "Use Git from the Windows Command Prompt" during installation. Otherwise, we believe it's OK to accept the defaults.
  * Note that RStudio for Windows prefers for Git to be installed below `C:/Program Files`, for example the Git executable on my Windows system is found at `C:/Program Files/Git/bin/git.exe`. Unless you have specific reasons to otherwise, follow this convention.
  


### Mac OS

**Option 1** (*highly recommended*): Install the Xcode command line tools (**not all of Xcode**), which includes Git. If your OS is older than 10.11 El Capitan, it is possible that you **must** install the Xcode command line tools in order for RStudio to find and use Git.

Go to the shell and enter one of these commands to elicit an offer to install developer command line tools:

``` 
git --version
git config
```

Accept the offer! Click on "Install".

Here's another way to request this installation, more directly:

``` 
xcode-select --install
```

We just happen to find this Git-based trigger apropos.

Note also that, after upgrading your Mac OS, you might need to re-do the above and/or re-agree to the Xcode license agreement. We have seen this cause the RStudio Git pane to disappear on a system where it was previously working. Use commands like those above to tickle Xcode into prompting you for what it needs, then restart RStudio.

**Option 2** (*recommended*): Install Git from here: <http://git-scm.com/downloads>.

  * This arguably sets you up the best for the future. It will certainly get you the latest version of Git of all approaches described here.
  * The GitHub home for this project is here: <https://github.com/timcharper/git_osx_installer>.
    - At that link, there is a list of maintained builds for various combinations of Git and Mac OS version. If you're running 10.7 Lion and struggling, we've had success in September 2015 with binaries found here: <https://www.wandisco.com/git/download>. 

**Option 3** (*recommended*): If you anticipate getting heavily into scientific computing, you're going to be installing and updating lots of software. You should check out [homebrew](http://brew.sh), "the missing package manager for OS X". Among many other things, it can install Git for you. Once you have Homebrew installed, do this in the shell:

```
brew install git
```



### Linux

Install Git via your distro's package manager.

Ubuntu or Debian Linux:

```sh
sudo apt-get install git
```

Fedora or RedHat Linux:

```sh
sudo yum install git
```

A comprehensive list for various Linux and Unix package managers:

<https://git-scm.com/download/linux>


## Test Installation

### R and RStudio
- Open RStudio
- Type `1 + 1` into the console. Does it show `[1] 2`? If so, good! If not, please ask your IT or a knowledgeable friend for help.

### Git and RStudio
- Open RStudio
- Click the "File" menu button, then "New Project".
- Click "Version Control".
- Click "Git".
- Copy-Paste the Repository URL (https://gitlab.com/MMS-Summer-School-2018/course-material) 
and decide in which directory the project should be stored. Click "create project".
- Go to the directory and check if it now contains a new directory called `course-material`.
- If this did not work, please ask your IT or a knowledgeable friend for help.







# Prepare your GitLab account

Please create a free account (core) on [GitLab](https://gitlab.com/)


# Connect RStudio to GitLab


High level overview of what must happen:

* Create a public-private SSH key pair. Literally, 2 special files, in a special place. 
* Add the private key to your ssh-agent. 
* Add your public key to your GitLab profile.

Using an SSH key is not required. You can also use HTTPS instead of SSH.
I like SSH better, because I do not have to enter my password all the time, so
it is just more convenient. This step, however, seems to be the most complex.
So if it does not work for you, you can use HTTPS until you find someone who can
help you :nerd_face: 

  
### Create SSH key pair

**Do you already have keys?**    
Go to *Tools > Global Options...> Git/SVN*. If you see something like `~/.ssh/id_rsa` in the `SSH RSA Key` box, you definitely have existing keys.

If not:

Go to *Tools > Global Options...> Git/SVN > Create RSA Key...*.

RStudio prompts you for a passphrase. It is optional, but also a best practice. Configuring your system for smooth operation with a passphrase-protected key introduces more moving parts. If you're completely new at all this, skip the passphrase and implement it next time, when you are more comfortable with system configuration. I did not use a passphrase at first, but I do now, and record it in a password manager.

Click "Create" and RStudio will generate an SSH key pair, stored in the files `~/.ssh/id_rsa` and `~/.ssh/id_rsa.pub`.


### Add key to ssh-agent

Tell your ssh-agent about the key and, especially, set it up to manage the passphrase, if you chose to set one.

Things get a little OS-specific around here. When in doubt, consult [GitHub's instructions for SSH](https://help.github.com/articles/connecting-to-github-with-ssh/), which is kept current for Mac, Windows, and Linux.

#### Mac OS

Make sure ssh-agent is enabled:

``` bash
eval "$(ssh-agent -s)"
```
You should get something like `Agent pid 6096`.

Add your key. If you set a passphrase, you'll be challenged for it here. Give it. The `-K` option stores your passphrase in the keychain.

``` bash
ssh-add -K ~/.ssh/id_rsa
```

If you're using a passphrase AND on macOS Sierra 10.12.2 and higher, you need to do one more thing. Create a file `~/.ssh/config` with these contents:

``` bash
Host *
 AddKeysToAgent yes
 UseKeychain yes
```

This should store your passphrase *persistently* in the keychain. Otherwise, you will have to enter it every time you log in. Useful StackOverflow thread: [How can I permanently add my SSH private key to Keychain so it is automatically available to ssh?](https://apple.stackexchange.com/questions/48502/how-can-i-permanently-add-my-ssh-private-key-to-keychain-so-it-is-automatically).

#### Windows

In RStudio go to the Terminal. To make sure ssh-agent is running, type:

``` bash
eval $(ssh-agent -s)
```
You should get something like `Agent pid 6096`.

Add your key.

``` bash
ssh-add ~/.ssh/id_rsa
```

#### Linux

In a shell, make sure ssh-agent is running:

``` bash
eval "$(ssh-agent -s)"
```
You should get something like `Agent pid 6096`.

Add your key.

``` bash
ssh-add ~/.ssh/id_rsa
```

### Provide public key to GitLab

Now we store a copy of your public key on GitLab

- In RStudio go to *Tools > Global Options...> Git/SVN*. Click "View public key" and copy the key to your clipboard.
- Sign into GitLab.
- Click on your profile pic on the top right corner on gitlab.com and select *Settings*. Then on the right click *SSH Keys*.
- Paste your public key in the *Key* box.
- Give the key an informative title (e.g. "work laptop 2018")
- Click *add*.

Test if it all worked by typing:
``` bash
ssh -T git@gitlab.com
```
in the `Terminal` in RStudio. If you are asked if you want to continue, type `yes`. 
In the end it should say something like `Welcome to GitLab, @HeidiSeibold!`. 
If this did not work, please ask your IT or a knowledgeable friend for help.

### Tell Git who you are

- Go to the `Terminal` in RStudio
- Type 
``` bash
git config --global user.email "you@example.com"
``` 
replace `you@example.com` with your e-mail address you used on GitLab.
- Type 
``` bash
git config --global user.name "YourName"
``` 
replace `Your Name` with your username on GitLab.


